# -*- coding: utf-8 -*-
from functools import wraps
import base64
import logging

from django.contrib.auth import authenticate, login
from jsonrpc.exceptions import JSONRPCDispatchException
from jsonrpc.backend.django import api

logger = logging.getLogger(__name__)


class NotAuthenticated(JSONRPCDispatchException):
    def __init__(self):
        JSONRPCDispatchException.__init__(
            self,
            code=32001,
            message='Authentication Required'
        )


class PermissionsDenied(JSONRPCDispatchException):
    def __init__(self):
        JSONRPCDispatchException.__init__(
            self,
            code=32002,
            message='You are not allowed to send messages to that user/chat'
        )


class DoesNotExist(JSONRPCDispatchException):
    def __init__(self):
        JSONRPCDispatchException.__init__(
            self,
            code=32010,
            message='User or chat does not exists'
        )


class UserDoesNotExist(DoesNotExist):
    def __init__(self):
        JSONRPCDispatchException.__init__(
            self,
            code=32011,
            message='User does not exists'
        )


class ChatDoesNotExist(DoesNotExist):
    def __init__(self):
        JSONRPCDispatchException.__init__(
            self,
            code=32012,
            message='Chat does not exists'
        )


def api_wrapper(func):
    @wraps(func)
    def _decorator(request, *args, **kwargs):
        if 'HTTP_AUTHORIZATION' in request.META:
            auth = request.META['HTTP_AUTHORIZATION'].split()
            if len(auth) == 2:
                if auth[0].lower() == "basic":
                    uname, passwd = base64.b64decode(auth[1]).decode().split(':')
                    user = authenticate(username=uname, password=passwd)
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                            request.user = user
        if not request.user.is_authenticated():
            raise NotAuthenticated
        return func(request, *args, **kwargs)

    api.dispatcher.add_method(_decorator, name=func.__name__)
    return _decorator
