# -*- coding: utf-8 -*-
import logging
import os
os.environ.setdefault("LDAPTLS_REQCERT","never")  # allows connection to self-signed ssl
from django_auth_ldap.backend import LDAPBackend
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger('django_auth_ldap.MyLDAPBackend')


class MyLDAPBackend(LDAPBackend):

    def authenticate(self, username, password, **kwargs):
        try:
            get_user_model().objects.get(username=username)
        except ObjectDoesNotExist:
            logger.debug("local user '{0}' not found".format(username))
#            return None
        else:
            logger.debug("local user '{0}' found".format(username))

        res = LDAPBackend.authenticate(self, username, password, **kwargs)
        logger.debug("user '{0}' ldap auth result: {1}".format(username, res))
        return res

    def get_or_create_user(self, username, ldap_user):
        logger.debug(
            "get_or_create_user, {0}, {1}".format(
                username,
                ldap_user))
        user_model = get_user_model()
        return user_model.objects.get_or_create(username=username)
