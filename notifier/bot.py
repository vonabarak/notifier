# -*- coding: utf-8 -*-

import traceback
from notifier.settings import TELEGRAM_TOKEN
from notifier.models import *
from notifier.schedule import get_all_users
from telegram import \
    InlineQueryResultArticle, ParseMode, InputTextMessageContent, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, InlineQueryHandler, CallbackQueryHandler, Filters
from telegram.error import Unauthorized
import logging
import json
import requests
import re
from uuid import uuid4

logger = logging.getLogger(__name__)
# suppress noisy messages about self-signed certificate
requests.packages.urllib3.disable_warnings()


def tgchat_from_message(message):
    if message.chat.type == 'private':
        internal_name = message.from_user.username
    elif message.chat.type == 'group':
        internal_name = message.chat.title
    elif message.chat.type == 'supergroup':
        internal_name = message.chat.title
    elif message.chat.type == 'channel':
        internal_name = message.chat.title
    else:
        logger.warning('Cannot get chat type')
        internal_name = str(uuid4())
    chat, created = TgChat.objects.get_or_create(
        tg_id=message.chat.id,
        defaults={
            'type': message.chat.type,
            'title': message.chat.title,
            'internal_name': internal_name,
        }
    )
    if created:
        logger.debug('Chat {0} created with id {1}'.format(chat.title, chat.id))
    return chat


def tgresponder_from_responder(responder):
    if responder.username:
        internal_name = responder.username
    elif responder.first_name:
        internal_name = responder.first_name
    else:
        logger.warning('Cant determine internal name for user with telegram id {0}. Using uuid'.format(responder.id))
        internal_name = str(uuid4())
    responder, created = TgResponder.objects.get_or_create(
        tg_id=responder.id,
        defaults={
            'first_name': responder.first_name,
            'last_name': responder.last_name,
            'username': responder.username,
            'internal_name': internal_name,
        }
    )
    if created:
        responder.chat  # auto-creating chat with responder
        logger.debug('Responder {0} created with id {1}'.format(responder.internal_name, responder.id))
    return responder


def pprint(msg):
    print('-' * 8 + 'MESSAGE' + '-' * 8)
    for i in msg:
        print(i)
    print('-' * 7 + 'ENDMESSAGE' + '-' * 6)


class BotUpdater(Updater):
    def __init__(self):
        Updater.__init__(self, token=TELEGRAM_TOKEN)
        self.dispatcher.add_handler(MessageHandler(Filters.text, self.on_message))
        self.dispatcher.add_handler(MessageHandler(Filters.status_update, self.on_status_update))
        self.dispatcher.add_handler(InlineQueryHandler(self.on_inline_query))
        self.dispatcher.add_handler(CallbackQueryHandler(self.on_callback_query))
        # self.dispatcher.add_handler(CommandHandler('on_message', self.on_message))
        self.responder, _ = TgResponder.objects.get_or_create(
            tg_id=253098291,
            defaults={
                'first_name': 'agava_notifier_bot',
                'last_name': None,
                'username': 'agava_notifier_bot',
                'type': 'bot',
                'internal_name': 'agava_notifier_bot',
            },
        )

    @staticmethod
    def on_status_update(_, update):  # unused variable for bot instance
        try:
            chat = tgchat_from_message(update.message)
            if update.message.new_chat_member:
                new_responder = tgresponder_from_responder(update.message.new_chat_member)
                chat.users.add(new_responder)
            elif update.message.left_chat_member:
                left_responder = tgresponder_from_responder(update.message.left_chat_member)
                chat.users.remove(left_responder)
            elif update.message.migrate_to_chat_id:
                chat.tg_id = update.message.migrate_to_chat_id
                chat.save()
            elif update.message.migrate_from_chat_id:
                chat.type = update.message.chat.type
                chat.save()
            else:
                tgchat_from_message(update.message)
        except BaseException as e:
            logger.warning('An exception occurred during receiving status update: {0}'.format(e), exc_info=True)

    @staticmethod
    def on_inline_query(bot, update):
        try:
            query = update.inline_query.query
            if 'incidents'.startswith(query):
                incidents = requests.get('https://monitoring.domain/api/incidents/?show=all', verify=False)
                results = [
                    InlineQueryResultArticle(
                        id=uuid4(),
                        title='All incidents',
                        input_message_content=InputTextMessageContent(
                            ''.join([
                                'id:\t {id}\n'
                                'hosts:\t {hosts}\n'
                                'services:\t {services}\n'
                                'assigned:\t {assigned_to}\n'
                                'minutes ago:\t {minutes_ago}\n'
                                '-----\n'.format(
                                    id=i['id'],
                                    hosts=i['hosts'],
                                    services=i['services'],
                                    assigned_to=i['assigned_to'],
                                    minutes_ago=i['minutes_ago'],
                                ) for i in incidents.json()
                            ])
                        )
                    )
                ]
                for i in incidents.json():
                    results.append(
                        InlineQueryResultArticle(
                            id=uuid4(),
                            title=' '.join(i['hosts']),
                            input_message_content=InputTextMessageContent(
                                'id:\t {id}\n'
                                'hosts:\t {hosts}\n'
                                'services:\t {services}\n'
                                'assigned:\t {assigned_to}\n'
                                'minutes ago:\t {minutes_ago}'.format(
                                    id=i['id'],
                                    hosts=i['hosts'],
                                    services=i['services'],
                                    assigned_to=i['assigned_to'],
                                    minutes_ago=i['minutes_ago'],
                                )
                            )
                        )
                    )
                update.inline_query.answer(results)
        except BaseException as e:
            logger.warning('An exception occurred during receiving inline query: {0}'.format(e), exc_info=True)
            update.inline_query.answer(
                [
                    InlineQueryResultArticle(
                        id=uuid4(),
                        title='Something goes wrong',
                        input_message_content=InputTextMessageContent('')
                    )
                ]
            )

    @staticmethod
    def on_callback_query(bot, update):
        try:
            query = update.callback_query
            responder = tgresponder_from_responder(query.from_user)
            chat = tgchat_from_message(query.message)
            message = Message.objects.get(chat=chat, tg_id=query.message.message_id)
            if query.data == 'assign':
                # Если юзер нажал "Назначить" заменяем кнопочки на список назначаемых
                bot.editMessageReplyMarkup(
                    chat_id=query.message.chat_id,
                    message_id=query.message.message_id,
                    reply_markup=InlineKeyboardMarkup([
                    [InlineKeyboardButton(i, callback_data='assign '+i),]
                        for i in get_all_users().keys()
                ] + [[InlineKeyboardButton('Отмена', callback_data='assign cancel'),]])
                )
            elif query.data.startswith('assign '):
                # Когда юзер выберет назначаемого, возвращаем кнопочки на место
                bot.editMessageReplyMarkup(
                    chat_id=query.message.chat_id,
                    message_id=query.message.message_id,
                    reply_markup=InlineKeyboardMarkup([
                        [
                            InlineKeyboardButton("Назначить", callback_data='assign'),
                            InlineKeyboardButton("Отложить", callback_data='delay'),
                        ],
                        [

                            InlineKeyboardButton("OK", callback_data='ok')
                        ],
                    ])
                )
            if query.data not in ('assign', 'assign cancel'):
                # creating fake message to use it as reply in NotifierUser's on_receive method
                reply = Message(
                    responder=responder,
                    chat=chat,
                    text=query.data,
                    reply_to=message,
                )

                message.notifier_user.on_receive(reply)
            bot.answerCallbackQuery(query.id, text=query.data)
        except BaseException as e:
            logger.warning('An exception occurred during processing callback: {0}'.format(e), exc_info=True)

    @staticmethod
    def on_message(_, update):  # unused variable for bot instance
        try:
            # adding responder and chat to database if they don't exists
            responder = tgresponder_from_responder(update.message.from_user)
            chat = tgchat_from_message(update.message)

            # actions if received message is a response to another message
            if update.message.reply_to_message:
                # adding responder and message to database if they don't exists
                reply_responder = tgresponder_from_responder(update.message.reply_to_message.from_user)
                reply, _ = Message.objects.get_or_create(
                    tg_id=update.message.reply_to_message.message_id,
                    chat=chat,
                    defaults={
                        'responder': reply_responder,
                        'text': update.message.reply_to_message.text,
                        'date': update.message.reply_to_message.date,
                    }
                )
            else:
                reply = None

            # saving message to db
            m = Message.objects.create(
                tg_id=update.message.message_id,
                responder=responder,
                chat=chat,
                text=update.message.text[:511],
                date=update.message.date,
                reply_to=reply,
            )

            # call on_receive method of NotifierUser if it was replied to NotifierUser's message
            if reply and reply.notifier_user:
                reply.notifier_user.on_receive(m)

        except BaseException as e:
            logger.warning('An exception occurred during receiving message: {0}'.format(e), exc_info=True)

    def msg(self, chat, text, user=None, disable_web_page_preview=True):
        try:
            if user and user.username == 'demo' and re.match(r'#i\d{6} Новый .*', text):
                inline_keyboard = InlineKeyboardMarkup([
                    [
                        InlineKeyboardButton("Назначить", callback_data='assign'),
                        InlineKeyboardButton("Отложить", callback_data='delay'),
                    ],
                    [

                        InlineKeyboardButton("OK", callback_data='ok')
                    ],
                ])
            else:
                inline_keyboard =None
            tg_msg = self.bot.sendMessage(
                chat_id=chat.tg_id,
                text=text,
                disable_web_page_preview=disable_web_page_preview,
                reply_markup=inline_keyboard
            )
            _msg = Message(
                tg_id=tg_msg.message_id,
                responder=self.responder,
                chat=chat,
                text=text[:1023],
                date=tg_msg.date,
                notifier_user=user,
            )
            _msg.save()
            return _msg
        except Unauthorized:
            logger.error(
                '{0} have not allowed bot to send private messages. '
                'Just send any text to bot via private message'.format(chat.internal_name), exc_info=True)
        except BaseException as e:
            logger.error(
                'An exception occurred during sending message to {0}: {1}'.format(chat.internal_name, e), exc_info=True)
