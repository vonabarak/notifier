# -*- coding: utf-8 -*-

from django import forms
from notifier.models import TgResponder,TgChat, NotifierUser


class SendMessageForm(forms.Form):
    # to = forms.ChoiceField(
    #     choices=[(i.id, i.title) for i in TgChat.objects.all()]
    # )
    body = forms.CharField(label='body', widget=forms.Textarea)

    def __init__(self, choices, *args, **kwargs):
        forms.Form.__init__(self, *args, **kwargs)
        self.fields['to'] = forms.ChoiceField(choices=choices)

class EditResponderForm(forms.ModelForm):
    # tg_id = forms.DecimalField(widget=forms.NumberInput(attrs={'disabled': True}), required=True)
    first_name = forms.CharField(widget=forms.TextInput(), required=False)
    last_name = forms.CharField(widget=forms.TextInput(), required=False)
    username = forms.CharField(widget=forms.TextInput(), required=False)
    phone_number = forms.RegexField(regex=r'^\d{5,20}$', required=False)
    # type = forms.CharField(widget=forms.TextInput(attrs={'disabled': True}), required=False)

    class Meta:
        model = TgResponder
        fields = [
        #    'tg_id',
            'first_name',
            'last_name',
            'username',
            'internal_name',
            'phone_number',
        #    'type',
        ]


class EditUserForm(forms.ModelForm):
    api_url = forms.RegexField(regex=r'^https?://.+$', required=False)
    class Meta:
        model = NotifierUser
        fields = ['username', 'is_staff', 'is_superuser', 'api_url']


class EditChatForm(forms.Form):
    name = forms.CharField(label='chat name', max_length=100)
    tg_id = forms.CharField(widget=forms.TextInput(attrs={'disabled': True}))
    description = forms.CharField(label='description', widget=forms.Textarea, required=False)
    api_url = forms.URLField(label='API URL', initial='http://', required=False)
    # group = forms.ChoiceField(
    #     choices=[(None, None)] + [(g.id, g.name) for g in Group.objects.all()],
    #     required=False
    # )
    # users = forms.MultipleChoiceField(
    #     choices=[(u.id, u.name) for u in TgUser.objects.filter(~Q(name=get_self().name))],
    #     # widget=forms.CheckboxSelectMultiple,
    #     help_text='select at least one',
    # )


class LoginForm(forms.Form):
    login = forms.CharField(label='login')
    password = forms.CharField(max_length=32, widget=forms.PasswordInput) 
