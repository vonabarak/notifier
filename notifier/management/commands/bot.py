# -*- coding: utf-8 -*-

import signal
from django.core.management.base import BaseCommand
from notifier.bot import BotUpdater


class Command(BaseCommand):
    help = 'Start consuming messages for telegram bot'
    updater = BotUpdater()

    def signal_handler(self, signal, frame):
        print('Stopping consumer')
        self.updater.stop()

    def handle(self, *args, **kwargs):
        signal.signal(signal.SIGINT, self.signal_handler)
        try:
            self.updater.start_polling()
        except IOError:
            pass