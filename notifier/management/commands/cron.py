from django.core.management.base import BaseCommand
from notifier.models import sync_chats_with_account, sync_all_chats_with_scheduler
from selfcheck.models import ErrorLog
from django.db.models import Q


class Command(BaseCommand):
    args = '[]'
    help = ''

    def handle(self, *args, **kwargs):
        ErrorLog.objects.filter(Q(viewed=False)).update(viewed=True)
        list(sync_chats_with_account())
        list(sync_all_chats_with_scheduler())
        # tg.sender(u'quit')
