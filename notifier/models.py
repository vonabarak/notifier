# -*- coding: utf-8 -*-

import json
import threading
import logging
import requests
from requests.exceptions import ConnectionError
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager, Group
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from django.utils import timezone

# suppress noisy messages about self-signed certificate
requests.packages.urllib3.disable_warnings()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class NotifierUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[
                                    validators.RegexValidator(r'^[\w.@+-]+$',
                                                              _('Enter a valid username. '
                                                                'This value may contain only letters, numbers '
                                                                'and @/./+/-/_ characters.'), 'invalid'),
                                ],
                                error_messages={
                                    'unique': _("A user with that username already exists."),
                                })
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    api_url = models.CharField(max_length=256, null=True)
    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username

    def on_receive(self, m):
        logger.debug(u'Message have been replied for notifier user {nuser}'
                     u' (api_url="{url}") by telegram user {tguser}'
                     u''.format(nuser=self.username, tguser=m.responder.internal_name, url=self.api_url))
        if self.api_url:
            request_msg = Message.objects.get(id=m.reply_to.id).as_dict()
            req = {
                'msg': m,
                'url': self.api_url,
                'data': json.dumps({
                    'message': m.as_dict(),
                    'request_msg': request_msg,
                }),
                'headers': {'content-type': 'application/json'},
            }

            class PostReqThread(threading.Thread):
                def __init__(self, request):
                    self.req = request
                    threading.Thread.__init__(self)

                def run(self):
                    logger.debug(u'Starting thread for post-request a message with id {id} to url: {url}'.format(
                        id=self.req['msg'].id,
                        url=self.req['url']
                    ))
                    try:
                        response = requests.post(
                            self.req['url'],
                            data=self.req['data'],
                            headers=self.req['headers'],
                            verify=False,
                            timeout=60
                        )
                        if not response.ok:
                            logger.warning(
                                u'Cannot post-request a message to URL {0} with error {1} {2}'.format(
                                    self.req['url'], response.status_code, response.reason))
                    except ConnectionError:
                        logger.warning(u'Cannot post-request a message to URL {0} because of connection error'.format(
                            self.req['url']))
            PostReqThread(req).start()


class TgResponder(models.Model):
    tg_id = models.BigIntegerField(unique=True)
    first_name = models.CharField(max_length=128, null=True)
    last_name = models.CharField(max_length=128, null=True)
    username = models.CharField(max_length=128, null=True)
    internal_name = models.CharField(max_length=128, unique=True, blank=False)
    phone_number = models.CharField(max_length=128, unique=True, null=True)
    type = models.CharField(max_length=32)

    @property
    def chat(self):
        _chat, _ = TgChat.objects.get_or_create(
            tg_id=self.tg_id,
            defaults={
                'title': self.internal_name,
                'internal_name': self.internal_name,
                'type': 'private',
            }
        )
        return _chat

    def __str__(self):
        return self.internal_name


class TgChat(models.Model):
    tg_id = models.BigIntegerField(unique=True)
    title = models.CharField(max_length=128)
    type = models.CharField(max_length=32)
    internal_name = models.CharField(max_length=128, unique=True, blank=False)
    users = models.ManyToManyField(TgResponder, related_name='chats')

    def __str__(self):
        return self.title


class Message(models.Model):
    tg_id = models.BigIntegerField(editable=False)
    responder = models.ForeignKey(TgResponder, related_name='messages')
    chat = models.ForeignKey(TgChat, related_name='messages')
    reply_to = models.ForeignKey('self', null=True)
    notifier_user = models.ForeignKey(NotifierUser, null=True, related_name='messages')
    text = models.CharField(max_length=512, null=True)
    date = models.DateTimeField()

    class Meta:
        unique_together = (('tg_id', 'chat'),)

    def as_dict(self):
        if self.reply_to:
            reply_id = self.reply_to.id
        else:
            reply_id = None

        if self.date:
            t = self.date.isoformat()
        else:
            t = None

        return {
            'id': self.id,
            'body': self.text,
            'sender': self.responder.internal_name,
            'recipient': self.chat.title,
            'time': t,
            'reply': reply_id,
            'tg_id': self.tg_id,
        }

    def __str__(self):
        return str(self.id)


# class TgResponder(models.Model):
#     name = models.CharField(max_length=64, unique=True)
#     tg_id = models.IntegerField(null=True, unique=True)
#     description = models.CharField(max_length=128, null=True)
#     group = models.ForeignKey(Group, null=True)
#
#     def is_chat(self):
#         try:
#             TgChat.objects.get(id=self.id)
#             return True
#         except TgChat.DoesNotExist:
#             return False
#
#     def check_permissions(self, django_user):
#         if django_user is not None:
#             if self.group is None:
#                 # there is no restrictions to send messages to this responder
#                 return True
#             else:
#                 if self.group in django_user.groups.all():
#                     # django user have right permissions to send messages to responder
#                     return True
#                 else:
#                     # django user cant send messages to responder
#                     return False
#         else:
#             # we just don't care of permissions
#             return True
#
#     def send(self, text, user=None):
#         if self.check_permissions(user):
#             if DONT_REALLY_SEND:
#                 if self.name not in DRS_ALLOWED:
#                     m = telegram.tg.send_msg(TgUser.objects.get(name=DRS_REDIRECT_USER), text, user)
#                     logger.info(u'Sending message to test user instead of  {} '
#                                 u'as DONT_REALLY_SEND is True'.format(self.name))
#                     return m
#             if self.tg_id:
#                 m = telegram.tg.send_msg(self, text, user)
#             else:
#                 m = Message(
#                     tg_id=None,
#                     incoming=False,
#                     auth_user=None,
#                     sender=get_self(),
#                     peer=self,
#                     recipient=self,
#                     reply=None,
#                     body=text,
#                 )
#                 m.save()
#                 logger.info(u'Cannot really send message to {} as responder have no telegram id. '
#                             u'Just write out the message to DB'.format(self.name))
#             return m
#         else:
#             logger.info(u'Message sending to {1} denied for {0}'.format(user, self.name))
#             return False
#
#     def cast(self):
#         try:
#             return TgChat.objects.get(id=self.id)
#         except TgChat.DoesNotExist:
#             return TgUser.objects.get(id=self.id)
#
#     def peer(self):
#         return self.cast().peer()
#
#
# class TgUser(TgResponder):
#     phone = models.CharField(max_length=64, null=True, unique=True)
#     vacation = models.BooleanField(default=False)
#
#     def peer(self):
#         return 'user#' + str(self.tg_id)
#
#     def rename(self, first_name, last_name):
#         self.name = first_name
#         self.save()
#         if not DONT_REALLY_SEND:
#             r = telegram.tg.sender(u'contact_rename {0} {1} {2}'.format(self.peer(), first_name, last_name))
#             if r['result'] == 'FAIL':
#                 logger.error(u'Cannot rename TgUser from {oldname} to {newname}. Response: {r}'.format(
#                     oldname=self.name,
#                     newname=str((first_name, last_name)),
#                     r=r
#                 ))
#
#
# class TgChat(TgResponder):
#     api_user = NotifierUser
#     api_url = models.TextField(null=True, max_length=512)
#     users = models.ManyToManyField(TgUser, related_name='chats')
#
#     def peer(self):
#         return 'chat#' + str(self.tg_id)
#
#     def add_user(self, user):
#         logger.debug(u'Adding user {0} to chat {1}'.format(user.name, self.name))
#         if user in self.users.all():
#             logger.debug(u'User {0} already in chat {1}'.format(user.name, self.name))
#         if self.tg_id:
#             # if tgchat have tg_id it is real chat in telegram account
#             if user.tg_id:
#                 # do nothing if user haven't tg_id
#                 if not DONT_REALLY_SEND or user.name in DRS_ALLOWED:
#                     r = telegram.tg.sender(u'chat_add_user {chat} {user}'.format(chat=self.peer(), user=user.peer()))
#                     if r['result'] == 'FAIL':
#                         logger.warning(u'Cannot add user {user} to chat {chat}: telegram response: {r}'.format(
#                             user=user.name,
#                             chat=self.name,
#                             r=r
#                         ))
#                         return False
#                 else:
#                     logger.info(u'dont really adding user {} chat {} '
#                                 u'as of DONT_REALLY_SEND'.format(user.name, self.name))
#         else:
#             # if tgchat haven't tg_id it is just an object in database but not a telegram chat
#             # so we have to create that chat
#             if user.tg_id:
#                 # do nothing if user haven't tg_id
#                 if not DONT_REALLY_SEND or user.name in DRS_ALLOWED:
#                     r = telegram.tg.sender(u'create_group_chat "{0}" {1}'.format(self.name, user.peer()))
#                     if 'result' not in r or r['result'] == 'FAIL':
#                         # failed to create chat
#                         logger.error(u'Cannot create chat "{name}". Response: {r}'.format(name=self.name, r=r))
#                     else:
#                         # chat created
#                         r = chat_info = telegram.tg.sender(u'chat_info {0}'.format(self.name.replace(' ', '_')))
#                         if 'result' not in r or r['result'] == 'FAIL':
#                             logger.error(u'Cannot get chat "{name}" info. Response: {r}'.format(r=r, name=self.name))
#                         else:
#                             self.tg_id = chat_info['id']
#                             self.name = chat_info['title']
#                             self.description = chat_info['print_name']
#                             self.save()
#                             logger.info(u'Chat {name} created with telegram id {id}'.format(name=self.name, id=self.id))
#                 else:
#                     logger.info(u'dont really adding user {} to chat {} '
#                                 u'as of DONT_REALLY_SEND'.format(user.name, self.name))
#         self.users.add(user)
#         logger.info(u'User {} have been added to chat {}'.format(user.name, self.name))
#         return True
#
#     def del_user(self, user):
#         # probably we don't want to delete ourselves from any chat
#         if user != get_self():
#             if user.tg_id:
#                 if not DONT_REALLY_SEND or user.name in DRS_ALLOWED:
#                     r = telegram.tg.sender(u'chat_del_user {chat} {user}'.format(chat=self.peer(), user=user.peer()))
#                     if r['result'] == 'FAIL':
#                         logger.error(u'''Cannot delete user {user} from chat {chat}: telegram response: {r}'''.format(
#                             user=user.name,
#                             chat=self.name,
#                             r=r
#                         ))
#                         return False
#                 else:
#                     logger.info(u'dont really deleting user {} from chat {} '
#                                 u'as of DONT_REALLY_SEND'.format(user.name, self.name))
#
#             self.users.remove(user)
#             logger.info(u'User {} have been removed from chat {}'.format(user.name, self.name))
#             return True
#
#     def flush(self):
#         # flush chat. delete all users from it
#         for tguser in self.users.all():
#             if self.del_user(tguser):
#                 time.sleep(1)
#
#     def on_receive(self, m):
#         logger.debug(u'Message have been sent to chat {chat} by user {user}'.format(
#             user=m.sender.name,
#             chat=m.peer.name
#         ))
#         if self.api_url:
#             if m.reply:
#                 request_msg = Message.objects.get(id=m.reply).as_dict()
#             else:
#                 request_msg = None
#             req = {
#                 'url': self.api_url,
#                 'data': json.dumps({
#                     'message': m.as_dict(),
#                     'request_msg': request_msg,
#                 }),
#                 'headers': {'content-type': 'application/json'},
#             }
#
#             class PostReqThread(threading.Thread):
#                 def __init__(self, request):
#                     self.req = request
#                     threading.Thread.__init__(self)
#
#                 def run(self):
#                     try:
#                         response = requests.post(
#                             self.req['url'],
#                             data=self.req['data'],
#                             headers=self.req['headers'],
#                             verify=False
#                         )
#                         if not response.ok:
#                             logger.warning(
#                                 u'Cannot post-request a message to URL {0} with error {1} {2}'.format(
#                                     self.req['url'], response.status_code, response.reason))
#                     except ConnectionError:
#                         logger.warning(u'Cannot post-request a message to URL {0} because of connection error'.format(
#                             self.req['url']))
#
#             PostReqThread(req).on_message()
#
#
# class Message(models.Model):
#     tg_id = models.IntegerField(null=True)
#     incoming = models.BooleanField(default=True)
#     auth_user = models.ForeignKey(NotifierUser, null=True, related_name='messages')
#     sender = models.ForeignKey(TgResponder, related_name='sender')
#     peer = models.ForeignKey(TgResponder, related_name='peer')
#     recipient = models.ForeignKey(TgResponder, related_name='recipient')
#     reply = models.ForeignKey('self', null=True)
#     body = models.TextField(null=False)
#     time = models.DateTimeField(auto_now_add=True)
#
#     def as_dict(self):
#         if self.reply:
#             reply_id = self.reply.id
#         else:
#             reply_id = None
#
#         if self.time:
#             t = self.time.isoformat()
#         else:
#             t = None
#
#         return {
#             'id': self.id,
#             'body': self.body,
#             'sender': self.sender.name,
#             'recipient': self.recipient.name,
#             'time': t,
#             'reply': reply_id,
#             'tg_id': self.tg_id,
#         }
#
#     def __str__(self):
#         return str(self.as_dict())
#
#
# def add_contact(phone, firstname, lastname='""'):
#     """Directly adds contact into cuurent telegram account's
#     contactlist and into database"""
#     logger.debug(u'add_contact "{0}" "{1}" "{2}"'.format(phone, firstname, lastname))
#     resp = telegram.tg.sender(u'add_contact {0} {1} {2}'.format(phone, firstname, lastname))
#     if resp:
#         j = resp[0]
#     else:
#         # cannot add user to contacts
#         logger.info(u'Cannot add user {user} with phone {phone} to contact list. '
#                     u'Probably this phone number '
#                     u'have no telegram account'.format(user=firstname, phone=phone))
#         raise TgUser.DoesNotExist
#     try:
#         tguser = TgUser.objects.get(phone=phone)
#         tguser.tg_id = j['id']
#         tguser.name = j['print_name']
#     except TgUser.DoesNotExist:
#         tguser = TgUser(
#             tg_id=j['id'],
#             name=j['print_name'],
#             phone=j['phone'],
#         )
#     tguser.save()
#     return tguser
#
#
# def create_chat(name, users):
#     """Creates new chat with specified users
#     Takes string as name of new chat and list of TgUser objects"""
#     telegram.tg.sender(u'create_group_chat ' + name + u' ' + u' '.join([u.peer() for u in users]))
#     r = telegram.tg.sender(u'chat_info {0}'.format(name))
#     if r['result'] == 'FAIL':
#         logger.error(u'Cannot get chat info from telegram. Response {0}'.format(r))
#     chat = responder_from_json(r).cast()
#     for u in users:
#         chat.users.add(u)
#     chat.save()
#     return chat
#
#
# def get_self():
#     """Returns TgUser object corresponding service's
#     telegram account"""
#     if 'tg_self_user' in globals():
#         return globals()['tg_self_user']
#     j = telegram.tg.sender(u'get_self')
#     try:
#         user = TgUser.objects.get(tg_id=j['id'])
#     except TgUser.DoesNotExist:
#         logger.warning(u'I dont exist )-: Let\'s try to create me')
#         user = TgUser(
#             tg_id=j['id'],
#             name=j['print_name'],
#             description=j['print_name'],
#             phone=j['phone'],
#         )
#         user.save()
#     globals()['tg_self_user'] = user
#     return user
#
#
# def responder_from_json(jresp):
#     """Takes json data in telegram-cli format and returns
#     TgChat or TgUser object based on id. Also creates responder
#     from this data if it is not exists"""
#
#     r = None
#     try:
#         r = TgResponder.objects.get(tg_id=jresp['id'])
#     except TgResponder.DoesNotExist:
#         try:
#             if 'print_name' in jresp:
#                 descr = jresp['print_name']
#             elif 'username' in jresp:
#                 descr = jresp['username']
#             else:
#                 descr = None
#
#             if jresp['type'] == 'chat':
#                 r = TgChat(
#                     tg_id=jresp['id'],
#                     description=descr,
#                     name=jresp['title'],
#                 )
#             elif jresp['type'] == 'user':
#                 r = TgUser(
#                     tg_id=jresp['id'],
#                     name=descr,
#                     description=descr,
#                     phone=jresp['phone'],
#                 )
#             r.save()
#         except KeyError:
#             logger.error(u'Cannot parse JSON:\n{0}'.format(jresp))
#     return r
#
#
# def tguser_from_scheduler_data(data):
#     """Takes user name and data in format of scheduler response
#     and creates new user if it doesn't exists
#     """
#     # logger.debug(u'tguser_from_scheduler_data\n {0}'.format(data))
#
#     # get phone number from 'phone' or from 'comment' field
#     if 'phone' in data:
#         phone = data['phone']
#     elif 'comment' in data:
#         phone = re.sub('\D', '', data['comment'])
#     else:
#         phone = None
#
#     # we can't add user if don't know it's name or phone
#     if not phone or 'name' not in data:
#         logger.info(u'Cannot add user {0} from scheduler. Name or phone unknown.'.format(data['name']))
#         raise TgUser.DoesNotExist
#     if data['name'] is None:
#         # scheduler don't know such user
#         raise TgUser.DoesNotExist
#
#     # trying to get tguser from DB by phone or by name
#     try:
#         # try to get user by phone
#         tguser = TgUser.objects.get(phone=phone)
#         tguser.name = data['name']
#     except TgUser.DoesNotExist:
#         # no user with dat phone, so lets try to search for user by name
#         try:
#             tguser = TgUser.objects.get(name=data['name'])
#         except TgUser.DoesNotExist:
#             # there is no user with dat phone number nor name
#             # so we can create user
#             if DONT_REALLY_SEND:
#                 tguser = TgUser(phone=phone, name=data['name'])
#             else:
#                 tguser = add_contact(phone, data['name'])
#
#     # check if user is on vacation
#     if 'vacation_until' in data:
#         try:
#             if datetime.datetime.strptime(data['vacation_until'], '%Y-%m-%d').date() > datetime.date.today():
#                 tguser.vacation = True
#             else:
#                 tguser.vacation = False
#         except ValueError:
#             tguser.vacation = False
#
#     tguser.save()
#     return tguser
#
#
# def sync_user_with_scheduler(user_name, force=False):
#     """Gets users phone number from scheduler via api and
#     adds this user to contact-list and database.
#     Returns TgUser instance."""
#     url = 'https://schedule.domain/api/contacts?name=' + user_name
#     # headers = {'content-type': 'application/json'}
#     if not force:
#         # we don't really want to sync just get a user object
#         try:
#             return TgUser.objects.get(name=user_name)
#         except TgUser.DoesNotExist:
#             # user doesn't exists. creating it from scheduler
#             pass
#     logger.debug(u'sync_user_with_scheduler {0}'.format(user_name))
#     try:
#         response = requests.get(url, verify=False).json()
#     except ConnectionError:
#         # no such user and cannot connect to scheduler
#         logger.warning(u'Cannot add user {0} from scheduler (ConnectionError)'.format(user_name))
#         raise TgUser.DoesNotExist
#     return tguser_from_scheduler_data(response)
#
#
# def tgchat_from_scheduler_data(data):
#     # logger.debug(u'tgchat_from_scheduler_data\n{0}'.format(data))
#     try:
#         # if required chat already exists
#         tgchat = TgChat.objects.get(name=data['name'])
#     except TgChat.DoesNotExist:
#         # chat doesn't yet exists. Don't really creating a chat in telegram,
#         # just creating an object in database. Chat will be created at first user addition
#         tgchat = TgChat(name=data['name'])
#         tgchat.save()
#
#     # adding users to chat
#     for u in data['users']:
#         try:
#             # try to get user from database
#             tguser = TgUser.objects.get(name=data['name'])
#         except TgUser.DoesNotExist:
#             # there is no such user in db. Trying to create it from scheduler data
#             try:
#                 tguser = tguser_from_scheduler_data(u)
#             except TgUser.DoesNotExist:
#                 # Don't know what to do with dat user. Just pass and continue
#                 continue
#         if tguser is not None and tguser not in tgchat.users.all():
#             # if user is not a member of chat than add it to chat
#             time.sleep(1)
#             logger.info(u'Adding user {0} into chat {1}'.format(tguser.name, tgchat.name))
#             tgchat.add_user(tguser)
#
#     # now remove from chat all users that don't present in scheduler for dis project
#     for tguser in tgchat.users.all():
#         if tguser.id == get_self().id:
#             # we don't want remove ourselves from chat anyway
#             continue
#         if tguser.name not in [u['name'] for u in data['users']]:
#             time.sleep(1)
#             logger.info(u'Removing user {0} from chat {1}'.format(tguser.name, tgchat.name))
#             tgchat.del_user(tguser)
#     return tgchat
#
#
# def sync_chat_with_scheduler(chat_name, force=False):
#     """Synchronize chat members with project members"""
#     if not force:
#         try:
#             return TgChat.objects.get(name=chat_name)
#         except TgChat.DoesNotExist:
#             # object doesn't exists. Lets try to create it
#             pass
#
#     url = 'https://schedule.domain/api/projects'
#     # headers = {'content-type': 'application/json'}
#     try:
#         response = requests.get(url, verify=False, timeout=10)
#         response.encoding = 'utf-8'
#         data = response.json()
#     except ConnectionError:
#         logger.exception(u'Cannot connect to scheduler.')
#         raise TgChat.DoesNotExist
#     project = None
#     for p in data:
#         if p['name'] == chat_name:
#             project = p
#     if project is None:
#         raise TgChat.DoesNotExist
#     return tgchat_from_scheduler_data(project)
#
#
# def sync_all_chats_with_scheduler():
#     """Synchronize chats and its members with projects and project's members"""
#     logger.info(u'\n\n\n{stars}\nSynchronizing contacts with scheduler...\n{stars}'.format(stars=u'*'*50))
#     url = 'https://schedule.domain/api/projects'
#     # headers = {'content-type': 'application/json'}
#     try:
#         response = requests.get(url, verify=False, timeout=10)
#         response.encoding = 'utf-8'
#         data = response.json()
#     except ConnectionError:
#         logger.exception(u'Cannot connect to scheduler.')
#         raise
#     for project in data:
#         yield (project['name'], tgchat_from_scheduler_data(project))
#     logger.info(u'Finished.\n{stars}\n'.format(stars=u'*'*50))
#
#
# def sync_chats_with_account():
#     logger.info(u'\n\n\n{stars}\nSynchronizing contacts with telegram account...\n{stars}'.format(stars=u'*'*50))
#
#     def add_user(x):
#         try:
#             u = TgUser.objects.get(tg_id=x['id'])
#         except TgUser.DoesNotExist:
#             if 'username' in x:
#                 username = x['username']
#             elif 'first_name'in x:
#                 username = x['first_name']
#             elif 'print_name'in x:
#                 username = x['print_name']
#             else:
#                 return None
#             if 'phone' in x:
#                 # probably only bot may haven't a phone
#                 (u, created) = TgUser.objects.get_or_create(phone=x['phone'])
#                 if created:
#                     logger.debug(u'get_contacts_from_tg: TgUser created by phone number {0}'.format(x['phone']))
#                     # user have been created by phone number. Lets try to assign a name to it
#                     try:
#                         # if there is a user with same name as just created then rename it
#                         olduser = TgUser.objects.get(name=username)
#                         olduser.name = u'old_' + username
#                         olduser.save()
#                     except TgUser.DoesNotExist:
#                         pass
#                     u.name = username
#             else:
#                 (u, created) = TgUser.objects.get_or_create(name=username)
#                 if created:
#                     logger.debug(u'get_contacts_from_tg: TgUser created with username {0}'.format(u.name))
#                 else:
#                     logger.debug(u'get_contacts_from_tg: TgUser with username {0} exists'.format(u.name))
#             u.tg_id = x['id']
#             if 'print_name' in x and not u.description:
#                 u.description = x['print_name']
#
#             u.save()
#         return u
#     for i in telegram.tg.sender(u'dialog_list'):
#         logger.debug(u'Processing dialog {0}'.format(i['print_name']))
#         if i['type'] == 'chat':
#             # if we got a chat
#             # try to find existing chat by telegram id
#             tgchat_ = TgChat.objects.filter(tg_id=i['id'])
#             if len(tgchat_) == 1:
#                 tgchat = tgchat_[0]
#             else:
#                 tgchat_ = TgChat.objects.filter(name=i['title'])
#                 if len(tgchat_) == 1:
#                     tgchat = tgchat_[0]
#                 else:
#                     tgchat = TgChat(
#                         tg_id=i['id'],
#                         name=i['title'],
#                         description=i['print_name'],
#                     )
#                     tgchat.save()
#             try:
#                 chat_info = telegram.tg.sender(u'chat_info {0}'.format(i['print_name']))
#             except (telegram.TelegramException, ValueError):
#                 logger.exception(
#                     u'Exception in adding users in chat {tgchat}. '
#                     u'Chat processing will be skipped'.format(tgchat=tgchat.name))
#                 continue
#             # logger.debug(u'chat_info:\n {0}\n\n'.format(chat_info))
#             if 'members' not in chat_info or chat_info['members_num'] == 0:
#                 # no members in dis chat
#                 tgchat.users.clear()
#                 yield tgchat
#                 continue
#             # removing users not presented in telegram's chat's members list
#             for tguser in tgchat.users.all():
#                 if tguser.tg_id not in [m['id'] for m in chat_info['members']]:
#                     logger.info(u'removing user {0} from chat {1}'.format(tguser.name, tgchat.name))
#                     tgchat.users.remove(tguser)
#             # adding users not presented in database
#             for m in chat_info['members']:
#                 if int(m['id']) == get_self().tg_id:
#                     continue
#                 tguser = add_user(m)
#                 if not tguser:
#                     continue
#                 if tguser not in tgchat.users.all():
#                     logger.info(u'adding user {0} to chat {1}'.format(tguser.name, tgchat.name))
#                     if tguser.id != 1:
#                         tgchat.users.add(tguser)
#             yield tgchat
#     for i in telegram.tg.sender(u'contact_list'):
#         logger.debug(u'Processing contact {0}'.format(i['print_name']))
#         yield add_user(i)
#     logger.info(u'Finished.\n{stars}\n'.format(stars=u'*'*50))
