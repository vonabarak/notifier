# -*- coding: utf-8 -*-

import logging
import requests
import re

# suppress noisy messages about self-signed certificate
requests.packages.urllib3.disable_warnings()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_all_users():
    result = requests.get('https://schedule.domain/api/projects', verify=False)
    result.encoding = 'utf-8'
    users = dict()
    for i in result.json():
        if 'users' in i:
            for j in i['users']:
                if 'name' in j:
                    users[j['name']] = re.sub(r'[^0-9+]','', j.get('comment'))
    return users

def add_responder(name=None, phone=None):
    if not (name or phone):
        return None



# d['phone'] = re.sub('[^0-9\+]','', d.get('comment'))