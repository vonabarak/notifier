# -*- coding: utf-8 -*-

import logging
from time import sleep
import threading
import socket
import json
import shlex
import subprocess
from notifier.settings import TELEGRAM_CLI

logger = logging.getLogger(__name__)


class TelegramException(BaseException):
    pass


class ConnectionError(TelegramException):
    pass


class IncorrectAnswer(TelegramException):
    pass


class TgCli:
    def __init__(self, host='localhost', port=4457):
        self._socket_used = threading.Semaphore(1)
        self.socket = None
        self.host = host
        self.port = port
        self.answer_header = b'ANSWER '
        self.main_session = b'main_session\n'

    def get_answer(self, timeout=10, blocking=False):
        with self._socket_used:
            _retry = 10
            _retry_timeout = 0.1
            self.socket.settimeout(timeout)
            if blocking:
                self.socket.setblocking(True)
            while _retry:
                # getting answer header wich must look like b'ANSWER 765\n' where 765 is answer size
                try:
                    header = self.socket.recv(len(self.answer_header))
                except socket.error as e:
                    if _retry:
                        logger.warning(u'Error reading from socket: {0}. retrying in {1} sec.'.format(
                            e, _retry_timeout))
                        _retry -= 1
                        sleep(_retry_timeout)
                        _retry_timeout += 1
                        continue
                    else:
                        logger.error(u'Error reading from socket: {0}.'.format(e))
                        raise
                if header != self.answer_header:
                    logger.warning(u'Incorrect answer from telegram-cli')
                    raise IncorrectAnswer
                # getting answer size
                buf = b''
                while True:
                    symbol = self.socket.recv(1)
                    if symbol == b'\n':
                        break
                    else:
                        buf += symbol
                answer_length = int(buf)

                answer = self.socket.recv(answer_length)

                buf = b''
                while True:
                    sym = self.socket.recv(1)
                    buf += sym
                    if sym == b'\n':
                        break
                if len(buf) > 1:
                    logger.info(u'Expected answer size: {0}, answer size: {1}.\n'
                                u'Dropped {2} symbols from answer:\n {3}\nanswer:{4}'
                                u''.format(
                                    answer_length,
                                    len(answer),
                                    len(buf),
                                    buf.decode('utf8'),
                                    answer.decode('utf8')))
                # if answer_length != len(answer):
                #     logger.warning(u'Answer length {0} doesn\'t match read answer {0}'.format(
                #         answer_length, len(answer)))
                return answer

    def connect(self):
        with self._socket_used:
            _retry = 10
            _timeout = 0.1
            while _retry:
                if self.socket:
                    self.socket.close()
                    self.socket = None
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                try:
                    self.socket.connect((self.host, self.port))
                except socket.error as e:
                    if _retry:
                        logger.warning(u'Cannot connect to socket {host}:{port}\n {e}. retrying in {timeout}'.format(
                            host=self.host,
                            port=self.port,
                            e=e,
                            timeout=_timeout
                        ))
                        sleep(_timeout)
                        _retry -= 1
                        _timeout += 1
                        continue
                    else:
                        logger.error(u'Cannot connect to socket {host}:{port}\n {e}'.format(
                            host=self.host,
                            port=self.port,
                            e=e
                        ))
                        raise
                return


class TgCliSender(TgCli):

    def _do_send(self, command):
        """Takes a binary data as argument, sends it to telegram-cli and returns
        binary data or telegram-cli's response"""
        self.connect()
        with self._socket_used:
            _retry = 5
            while _retry:
                _retry_timeout = 1
                try:
                    self.socket.send(command+b'\n')
                except socket.error as e:
                    if _retry:
                        logger.warning(u'Cannot send to socket: {0} retry in {1}'.format(e, _retry_timeout))
                        sleep(_retry_timeout)
                        _retry -= 1
                        _retry_timeout += 5
                        continue
                    else:
                        logger.error(u'Cannot send to socket: {0}'.format(e))
                        raise
                # we do not expect any response if we've sent quit
                if command == b'quit':
                    return '"ok"'
                break
        return self.get_answer()

    def __call__(self, command):
        logger.debug(u'Sending command to telegram-cli: {0}'.format(command))
        try:
            cmd = command.encode('utf8')
        except UnicodeEncodeError as e:
            logger.error(u'Error trying to execute command:\n {0}'.format(e))
            raise
        try:
            ret = json.loads(self._do_send(cmd).decode('utf8'))
        except UnicodeDecodeError as e:
            logger.error(u'Error trying to execute command:\n {0}'.format(e))
            raise
        except ValueError as e:
            logger.error(u'Error trying to execute command:\n {0}'.format(e))
            raise
        logger.debug(u'telegram-cli response:\n{0}'.format(ret))
        return ret


class TgCliReceiver(TgCliSender):
    def __init__(self, *args, **kwargs):
        TgCliSender.__init__(self, *args, **kwargs)
        self._quit = None

    def stop_generator(self):
        logger.debug(u'Quitting receiver')
        self._quit = True

    def _do_recv(self):
        logger.debug(u'Starting receiver')
        self._quit = False
        self.connect()
        _retry = 10
        _retry_timeout = 0.1
        while not self._quit:
            with self._socket_used:
                try:
                    self.socket.sendall(self.main_session)
                except Exception as e:
                    if _retry:
                        logger.warning(u'Cannot send data to socket. Exception: {0}\n retry in {1}'.format(
                            e, _retry_timeout))
                        sleep(_retry_timeout)
                        _retry_timeout += 1
                        _retry -= 1
                        continue
                    else:
                        logger.error(u'Cannot send data to socket.')
                        raise ConnectionError
            yield self.get_answer(blocking=True)

    def generator(self):
        self._quit = False
        _retry = 10
        while not self._quit:
            try:
                gen = self._do_recv()
                while not self._quit:
                    msg = next(gen)
                    yield json.loads(msg.decode('utf8'))
            except BaseException as e:
                errmsg = u'Error in yielding message: {0}. Retrying...'.format(e)
                if _retry == 0:
                    logger.error(errmsg)
                    _retry = 10
                else:
                    logger.warning(errmsg)
                    _retry -= 1
                continue


class Telegram:
    def __init__(self):
        self.subproc = subprocess.Popen(shlex.split(
            '{path} -W --json -P{port}'.format(
                path=TELEGRAM_CLI['path'],
                port=TELEGRAM_CLI['port'],
            )
        ), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.sender = TgCliSender(host=TELEGRAM_CLI['host'], port=TELEGRAM_CLI['port'])
        # self.receiver = TgCliReceiver(host=TELEGRAM_CLI['host'], port=TELEGRAM_CLI['port'])
        self.sender(u'status_online')
