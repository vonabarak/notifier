# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
from jsonrpc.backend.django import api
from notifier.views import *

admin.autodiscover()

urlpatterns = [
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),
    url(r'^admin/', include(admin.site.urls), name='admin'),
    url(r'^accounts/login/$', LoginView.as_view(), name='login'),
    url(r'^accounts/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^$', HomePageView.as_view(), name='home'),

    url(r'^messages$', MessagesView.as_view(), name='messages'),
    url(r'^chats$', ChatsView.as_view(), name='chats'),
    url(r'^responders$', RespondersView.as_view(), name='responders'),
    url(r'^editresponder/(?P<pk>\d+)$', EditResponderView.as_view(), name='editresponder'),
    url(r'^sendmessage$', SendMessageView.as_view(), name='sendmessage'),

    url(r'^edituser/(?P<pk>\d+)$', EditUserView.as_view(), name='edituser'),
    url(r'^log/$', LogView.as_view(), name='log'),

    url(r'^api$', ApiView.as_view(), name='api'),
    url(r'^changelog$', ChangelogView.as_view(), name='changelog'),
    url(r'^selfcheck/nagios$', NagiosCheckView.as_view(), name='selfcheck_nagios'),
    url(r'^selfcheck/reset$', ResetErrorsView.as_view(), name='selfcheck_reset'),
    # API
    url(r'^jsonrpc/$', api.jsonrpc, name='jsonrpc'),
    url(r'^jsonrpc/map/$', api.jsonrpc_map, name='jsonrpc_map'),
]
