# -*- coding: utf-8 -*-
import json
import logging
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.generic import FormView
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import UpdateView
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, render_to_response
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import logout, login, authenticate
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from .forms import *
from notifier.models import NotifierUser, TgResponder, TgChat, Message
from notifier.apiwrapper import api_wrapper, PermissionsDenied, UserDoesNotExist, ChatDoesNotExist
from notifier.bot import BotUpdater
from selfcheck.models import StatusLog

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class PaginationMixin:
    def __init__(self):
        self.lines = list()
        self.request = None

    def add_paginator(self, context, lines):
        rows = 20
        paginator = Paginator(lines, rows)
        page = self.request.GET.get('page', '1')
        try:
            show_lines = paginator.page(page)
        except PageNotAnInteger:
            show_lines = paginator.page(1)
        except EmptyPage:
            show_lines = paginator.page(paginator.num_pages)
        context['lines'] = show_lines
        context['rows'] = rows
        context['page'] = page


class HomePageView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        # messages.info(self.request, 'This is a demo of a message.')
        return context


class LogView(TemplateView, PaginationMixin):
    template_name = 'log.html'

    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        level = self.request.GET.get('level', '0')
        logger = self.request.GET.get('logger', '')
        context['level'] = level
        context['logger'] = logger
        if logger:
            lines = StatusLog.objects.filter(level__gte=level, logger_name=logger).order_by('-id')
        else:
            lines = StatusLog.objects.filter(level__gte=level).order_by('-id')
        self.add_paginator(context, lines)
        return context


class MessagesView(TemplateView, PaginationMixin):
    template_name = 'messages.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        self.add_paginator(context, Message.objects.all().order_by('-id'))
        return context


class ChatsView(TemplateView, PaginationMixin):
    template_name = 'chats.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        self.add_paginator(context, TgChat.objects.all().order_by('internal_name'))
        return context


class RespondersView(TemplateView, PaginationMixin):
    template_name = 'responders.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        self.add_paginator(context, TgResponder.objects.all().order_by('internal_name'))
        return context


class EditResponderView(UpdateView):
    model = TgResponder
    template_name = 'form.html'
    form_class = EditResponderForm
    success_url = reverse_lazy('responders')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)


class EditUserView(UpdateView):
    model = NotifierUser
    template_name = 'form.html'
    form_class = EditUserForm
    success_url = reverse_lazy('home')

    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)


class SendMessageView(FormView):
    template_name = 'sendmessage.html'
    form_class = SendMessageForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get_form(self):
        form_class = self.get_form_class()
        to = int(self.request.GET.get('id', 0))
        if to:
            return form_class(
                choices=[(i.id, i.internal_name) for i in TgChat.objects.all()],
                initial={
                    'to': to,
                }
            )
        else:
            return form_class(choices=[(i.id, i.internal_name) for i in TgChat.objects.all()])

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context

    def post(self, request, *args, **kwargs):
        chat_id = int(request.POST.get('to', 0))
        text = request.POST.get('body', '')
        chat = get_object_or_404(TgChat, id=chat_id)
        if BotUpdater().msg(chat=chat, text=text, user=request.user):
            messages.info(self.request, 'Message sent.')
        else:
            messages.warning(self.request, 'Cannot send message')
        return HttpResponseRedirect(reverse('sendmessage'))


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context

    def post(self, request, **kwargs):
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.info(request, 'Successfuly logged in')
            else:
                messages.info(request, 'Account disabled')
        else:
            messages.warning(request, 'Incorrect login')
        redirect = request.GET.get('next', False)
        if redirect:
            return HttpResponseRedirect(redirect)
        else:
            return render(request, self.template_name, self.get_context_data(**kwargs))


class LogoutView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        messages.info(request, 'Good Bye!')
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class ApiView(TemplateView):
    template_name = 'api.html'


class ChangelogView(TemplateView):
    template_name = 'changelog.html'


class NagiosCheckView(View):

    @staticmethod
    def get(request):
        errors = StatusLog.objects.filter(Q(level__gte=40) & Q(viewed=False))
        warnings = StatusLog.objects.filter(Q(level=30) & Q(viewed=False))
        if errors:
            result = u''.join(
                [u'{ctime}: {msg}\n'.format(msg=e.msg, ctime=e.ctime.isoformat()) for e in errors]
            )
            code = "CRITICAL"
        elif warnings:
            result = u''.join(
                [u'{ctime}: {msg}\n'.format(msg=e.msg, ctime=e.ctime.isoformat()) for e in warnings]
            )
            code = "WARNING"
        else:
            result = "Sun is shining, weather is sweet."
            code = "OK"
        return HttpResponse(json.dumps({"result": result, "code": code}))


class ResetErrorsView(View):
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(self.__class__, self).dispatch(*args, **kwargs)

    @staticmethod
    def get(request):
        StatusLog.objects.filter(Q(viewed=False)).update(viewed=True)
        return HttpResponseRedirect(reverse('selfcheck_nagios'))

###############################################################################
# API views


@api_wrapper
def user_message(request, to, text):
    """Sends message with text "text" to user "to"
    :return: message_id
    """
    logger.debug(u'user_message({0}, {1}) called'.format(to, text))
    try:
        tguser = TgResponder.objects.get(internal_name=to)
    except TgResponder.DoesNotExist:
        raise UserDoesNotExist
    result = BotUpdater().msg(tguser.chat, text, request.user)
    if result:
        return result.id
    else:
        raise PermissionsDenied


@api_wrapper
def chat_message(request, to, text):
    """Sends message with text "text" to chat "to"
    :return: message_id
    """
    logger.debug(u'chat_message({0}, {1}) called'.format(to, text))
    try:
        tgchat = TgChat.objects.get(internal_name=to)
    except TgChat.DoesNotExist:
        raise ChatDoesNotExist
    result = BotUpdater().msg(tgchat, text, request.user)
    if result:
        return result.id
    else:
        raise PermissionsDenied


@api_wrapper
def get_messages(request, recipient=None, sender=None, time_from=None, limit=None):
    """Returns a list of messages that satisfy requested parameters
    :arg
    recipient (str): recipient of message
    sender (str): sender of message
    time_from (datetime): minimal date and time of sending message
    limit (str): maximal count of messages to get
    :return: messages list
    """
    logger.debug(u'get_messages() called')
    query = Q()
    if time_from:
        query.add(Q(date__gt=time_from), Q.AND)
    if recipient:
        query.add(Q(chat__internal_name=recipient), Q.AND)
    if sender:
        query.add(Q(responder__internal_name=sender), Q.AND)
    msgs = Message.objects.filter(query).order_by('-date')[:limit]
    return [i.as_dict() for i in msgs]


@api_wrapper
def get_message(request, msg_id):
    """Returns message by it's id
    :arg
    msg_id (int): message id
    :return: message "object" (it is dictionary in python's terminology)
    """
    return Message.objects.get(id=msg_id).as_dict()
